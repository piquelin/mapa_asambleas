# El Mapa de las Asambleas: Plan de Avance

Armamos informe de reunión del 9/4, a modo de propuesta de plan de avance. En la reunión después de una presentación y repaso del trabajo realizado hasta el momento, realizamos un intercambio sobre la situación del proyecto y sus perspectivas. 

Luego hicimos un repaso de distintos aspectos del proyecto, a los que agregamos propuestas de funcionamiento coordinar mejor las distintas tareas involucradas y como plan de avance. 

## 1) Carga y chequeo de datos

Una de las tareas más importantes y que más tiempo lleva del proyecto es el chequeo y actualización de la información de las asambleas. Esta tarea no requiere conocimientos técnicos y está abierta a la colaboración de asambleístas que participaron de la reunión y a quienes quieran colaborar con el proyecto. Donde lo más importante a actualizar son los horarios y lugares de las asambleas. Además de revisar y avisar sobre posibles errores.

Donde lo más importante a actualizar son los horarios y lugares de las asambleas. Además de revisar y avisar sobre posibles errores.

Promovemos que cada asamblea chequee su información y de las asambleas que conozca. Priorizando el chequeo de las actualizaciones de las asambleas próximas a realizarse. Para actualizar la información sugerimos utilizar los mapas o el siguiente [listado de asambleas](https://mapadelasasambleas.com/listado.html) que se mantiene actualizado.

Nos pueden avisar las modificaciones de manera directa por mensaje de texto en caso de tener contacto directo con quienes mantenemos el mapa o por las siguientes vías de contacto:
- [Formulario de Carga](https://mapadelasasambleas.com/formulario.html): para carga de nuevas asambleas o avisando en observaciones que datos se actualizan.
- Correo: mapadelasasambleas@gmail.com 
- [Instagram](https://www.instagram.com/mapadelasasambleas)
- [Twitter](https://twitter.com/MapadeAsambleas)
- [Grupo de Telegram](https://t.me/+RvmuzJhSLFRmI27t)

## 2) Mapeo y actualización de mapas y sitio

La siguiente tarea es actualizar el mapa y el sitio con las novedades.

### Revisión principal del listado

- Revisar todas las vía de contacto para ver avisos de novedades que nos pudieran mandar. Responder siempre a los avisos para que sepan que lo recibimos y nos avisen en próximas oportunidades.
- Luego revisar el [listado de asambleas](https://mapadelasasambleas.com/listado.html) y las actualizaciones por Instagram, para chequear sólo posibles cambios en horarios y lugares de asambleas. 
- Actualizar la hoja de cálculo del [Listado de Asambleas publicadas y mapeadas](https://docs.google.com/spreadsheets/d/1TIGvJzj3d3Ib3jbQAV8vQL2j5F8Fkc04AoyetWpSUJM)
- Avisar: Una vez actualizadas todas las novedades, lunes por la tarde/noche, avisar  a lxs responsables de los mapas y el sitio para que los actualicen. 

Esto es el mayor trabajo, por lo que proponemos una sola revisión semanal, que puede ser **desde el domingo por la noche hasta el lunes por la tarde**. Y podemos repartirnos las asambleas a chequear entre varixs.

### Revisión permanente de avisos

También estaremos revisando avisos de novedades durante la semana. Actualizando la hoja de cálculo durante el día. En el caso de haber novedades actualizamos mapas y sitio por las noches. Aunque no podamos garantizar esto todos los días, intentaremos realizarlo lo más regularmente posible porque no implica tanto trabajo.

## 3) Difusión por redes sociales

La tarea de difusión por redes sociales es fundamental porque es lo que llega del mapa a las asambleas y es importante mantenerla en lo posible de manera sistemática.

- Seguir a todas las asambleas por Instagram. Proponemos seguir sólo a las asambleas del Mapa, no a personas individuales para facilitar el seguimiento de las novedades.
- Darle me gusta a las publicaciones de todas las asambleas (sólo las más recientes y vigentes).
- Chequear sólo horarios y lugares de asambleas para verificar que coincidan con el [listado de asambleas del sitio](https://mapadelasasambleas.com/listado.html). 
- En caso de encontrar novedades en los horiarios o lugares, avisar para que se actualice el dato. O modificarlo directamente en la hoja de cálculos pública y avisar para que por la noche actualicen mapas y sitio.
- Intentar contestar mensajes y avisos de actualizaciones que nos manden, para que se sepa que ya lo vimos y tomamos nota y mantener la relación con colaboradorxs y asambleas.
- En la red social X, no parece necesario seguir a asambleas porque muy pocas tiene cuentas acá. 
- Intentar tuitear la información de las asambleas que vayan a realizarse ese día, esto puede hacerse desde el listado https://mapadelasasambleas.com/listado.html donde la opción X permite publicar con la posibilidad de incluir la imágen de la asamblea en el tuit, mejorar mensaje y por ejemplo el hashtag #Asamblea #Localidad

## Mejoras técnicas

No pudimos abordar en profundidad las mejoras y tareas técnicas que tenemos pendientes, pero como propuesta para organizarnos en esto podemos ir cargando en adelante en tickets del proyecto para mejor seguimiento: https://gitlab.com/piquelin/mapa_asambleas/-/issues

## Gráfico del proyecto

![Gráfico del proyecto El Mapa de las Asambleas](grafico_proyecto.jpg)

## Mapa de la Resistencia

Charlamos sobre la necesidad que muchas compañeras y compañeros nos hacen llegar de reflejar distintas luchas tanto para darles visibilidad como para promover la coordinación de las mismas, donde las asambleas en particular pueden juntar un importante rol. Intercambiamos sobre la posibilidad de realizar otros mapas como podrían ser mapa de despidos, mapa de conflividad social, nos parecía que "El Mapa de la Resistencia" podía ser un nombre que aglutine y le de un sentido de lucha a los conflictos.

Quedamos en seguir priorizando el mapa de las asambleas, pero en la medida de lo posible ir dando pasos y quizás una próxima reunión que incluya cuestiones específicas del tema.

Sabemos que no es nada fácil, ya bastante trabajo de chequeo, actualización, difusión y mejoras tenemos con el Mapa de las Asambleas, un mapa más ambicioso todavía seguramente implicará un enorme trabajo en este sentido. Hay que tener en cuenta por ejemplo que esto puede incluir luchas de todo tipo, como despidos, cierres, paros, marchas, cortes, etc de diversidad de sectores tanto de trabajadores, estudiantes, diversidades, represión, etc. de diversas fuentes. Esto implica cierta información que permitan una clasificación, y preparar bien la estructura de datos antes que nada que es lo más importante.

Pero en principio la idea es intentar replicar un esquema similar al que venimos usando en el Mapa de las Asambleas, donde los spreadsheets pueden funcionar como "fuentes de información" (backends) que pueden consultarse y descargarse periódicamente de manera pública, ver por ejemplo https://gitlab.com/piquelin/mapa_asambleas/-/blob/desarrollo/generar.sh?ref_type=heads#L65 y (podría ser otro similar del mapa de la resistencia). Y luego manejarse por las "visualizaciones o mapas" (frontends) que cada uno maneje y/o desarrolle, ya sea en un mismo mapa, en varios o en distintas capas. Donde lo más importante es la calidad de la información chequeada y curada que tengamos, que es lo que permite alimentar los distintos mapas.

## Próximas reuniones

Quedamos en definir próximas reuniones ya sea técnicas de trabajo o talleres abiertos como la propuesta que sigue en pie de hacer uno en Telam sobre el Mapa de las Asambleas e introducción a los mapas, pendiente de definir junto a las y los trabajadores.
