#!/usr/bin/env python
# coding: utf-8

import pandas as pd
import geopandas as gpd
from shapely.geometry import Point
import folium

def cargar_datos(url):
    df_asambleas = pd.read_csv(url)
    df_asambleas = df_asambleas[df_asambleas['Longitud'].notna()]
    return df_asambleas

def crear_geodataframe(df):
    geometry = [Point(xy) for xy in zip(df['Longitud'], df['Latitud'])]
    asambleas_geo = gpd.GeoDataFrame(df, geometry=geometry)
    return asambleas_geo

def crear_mapa(asambleas_geo):

    # Crear el mapa de Folium
    m = folium.Map(location=[-34.67, -58.6], # elijo el centro para el mapa inicial
                   zoom_start=11,
                   tiles=None) # sacamos el mapa por defecto porque no reconoce Las Malvinas como argentinas

    # Uso este mapa porque bancamos al IGN onde están despidiendo gente
    folium.raster_layers.TileLayer(tiles='https://wms.ign.gob.ar/geoserver/gwc/service/tms/1.0.0/mapabase_gris@EPSG%3A3857@png/{z}/{x}/{-y}.png',
                                   attr='<a href="http://www.ign.gob.ar/AreaServicios/Argenmap/IntroduccionV2" target="_blank">Instituto Geográfico Nacional</a> + <a href="http://www.osm.org/copyright" target="_blank">OpenStreetMap</a>',
                                   name='Argenmap (gris) - IGN').add_to(m)

    folium.raster_layers.TileLayer(tiles='https://wms.ign.gob.ar/geoserver/gwc/service/tms/1.0.0/mapabase_topo@EPSG%3A3857@png/{z}/{x}/{-y}.png',
                                   attr='<a href="http://www.ign.gob.ar/AreaServicios/Argenmap/IntroduccionV2" target="_blank">Instituto Geográfico Nacional</a> + <a href="http://www.osm.org/copyright" target="_blank">OpenStreetMap</a>',
                                   name='Argenmap (topográfico) - IGN').add_to(m)

    folium.raster_layers.TileLayer(tiles='https://wms.ign.gob.ar/geoserver/gwc/service/tms/1.0.0/capabaseargenmap@EPSG%3A3857@png/{z}/{x}/{-y}.png',
                                   attr='<a href="http://www.ign.gob.ar/AreaServicios/Argenmap/IntroduccionV2" target="_blank">Instituto Geográfico Nacional</a> + <a href="http://www.osm.org/copyright" target="_blank">OpenStreetMap</a>',
                                   name='Argenmap - IGN').add_to(m)

    folium.raster_layers.TileLayer(tiles='https://wms.ign.gob.ar/geoserver/gwc/service/tms/1.0.0/argenmap_oscuro@EPSG%3A3857@png/{z}/{x}/{-y}.png',
                                   attr='<a href="http://www.ign.gob.ar/AreaServicios/Argenmap/IntroduccionV2" target="_blank">Instituto Geográfico Nacional</a> + <a href="http://www.osm.org/copyright" target="_blank">OpenStreetMap</a>',
                                   name='Argenmap (oscuro) - IGN').add_to(m)

    asambleas_layer = folium.FeatureGroup(name='Asambleas')

    columnas_para_incluir = ['Nombre de Asamblea', 'Dirección de la Asamblea',
                             'Día y hora de la asamblea', 'Descripción breve',
                             'Contactos', 'X (Twitter)', 'Instagram', 'Facebook',
                             'TikTok', 'Última actualización']

    for idx, row in asambleas_geo.iterrows():
        popup_content = ""
        for columna in columnas_para_incluir:
            if pd.notnull(row[columna]):  # Verificar que el valor no sea nulo
                if columna == 'Instagram':
                    popup_content += f"<b>Sitio:<a href=https://www.instagram.com/{row['Instagram'][1:]} target=_blank> </b> {row['Instagram']}</a><br>"
                else:
                    popup_content += f"<b>{columna}:</b> {row[columna]}<br>"

        if pd.notnull(row['Sitio']):  # Verificar que el valor no sea nulo
            # Agrega el sitio como hipervínculo
            popup_content += f"<b>Sitio:<a href={row['Sitio']} target=_blank> </b> {row['Sitio']}</a><br>"

        folium.CircleMarker(
            location=[row.geometry.y, row.geometry.x],
            radius=7,
            fill=True,
            color='red',
            fill_opacity=0.3,
            popup=folium.Popup(popup_content, max_width=500),
            tooltip=f"{row['Nombre de Asamblea']}",
        ).add_to(asambleas_layer)

    asambleas_layer.add_to(m)
    folium.LayerControl().add_to(m)
    return m

def guardar_mapa(mapa, filename):
    mapa.save(filename)

if __name__ == "__main__":
    url = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vTixuJvMIZjfRD8Vo9TUBd4aWqzB2rgjFgdGyXaGAwfLIWuj71bN5GI2DzC9g1ROuBSdtco6naDdvQq/pub?gid=190070517&single=true&output=csv'
    datos = cargar_datos(url)
    geodf = crear_geodataframe(datos)
    mapa = crear_mapa(geodf)
    guardar_mapa(mapa, '../sitio/asambleas_mapa_sitio.html')
