# Reemplazar en index de asamblea
reemplazar() {
    fila="$1"

    url=$(echo "$fila" | awk -F'\t' '{print $17}')
    archivohtml=$(echo "$fila" | awk -F'\t' '{print $17}'|xargs basename|sed 's/[^[:alnum:]]//g')
    archivohtml="${archivohtml:0:$((-4))}.${archivohtml:$((-4))}"
    archivo="tmp/$archivohtml"
    cp index_asamblea.html $archivo

    nombre_asamblea=$(echo "$fila" | awk -F'\t' '{print $2}'|sed 's/[\/&]/\\&/g')
    provincia=$(echo "$fila" | awk -F'\t' '{print $3}'|sed 's/[\/&]/\\&/g')
    localidad=$(echo "$fila" | awk -F'\t' '{print $4}'|sed 's/[\/&]/\\&/g')
    dir_asamblea=$(echo "$fila" | awk -F'\t' '{print $5}'|sed 's/[\/&]/\\&/g')
    horario=$(echo "$fila" | awk -F'\t' '{print $6}'|sed 's/[\/&]/\\&/g')
    descripcion=$(echo "$fila" | awk -F'\t' '{print $7}'|sed 's/[\/&]/\\&/g')
    foto=$(echo "$fila" | awk -F'\t' '{print $8}'|sed 's/[\/&]/\\&/g')
    contactos=$(echo "$fila" | awk -F'\t' '{print $9}'|sed 's/[\/&]/\\&/g')
    mapa=$(echo "$fila" | awk -F'\t' '{print $10}'|sed 's/[\/&]/\\&/g')
    twitter=$(echo "$fila" | awk -F'\t' '{print $11}'|sed 's/[\/&]/\\&/g')
    instagram=$(echo "$fila" | awk -F'\t' '{print $12}'|sed 's/[\/&]/\\&/g')
    facebook=$(echo "$fila" | awk -F'\t' '{print $13}'|sed 's/[\/&]/\\&/g')
    tiktok=$(echo "$fila" | awk -F'\t' '{print $14}'|sed 's/[\/&]/\\&/g')
    dominio=$(echo "$fila" | awk -F'\t' '{print $17}'|sed 's/[\/&]/\\&/g')

    sinarrobai="${instagram:1}"
    sinarrobat="${twitter:1}"

    sed -i "s/NOMBRE_ASAMBLEA/$nombre_asamblea/g" $archivo
    sed -i "s/PROVINCIA/$provincia/g" $archivo
    sed -i "s/LOCALIDAD/$localidad/g" $archivo
    sed -i "s/DIR_ASAMBLEA/$dir_asamblea/g" $archivo
    sed -i "s/HORARIO/$horario/g" $archivo
    sed -i "s/DESCRIPCION/$descripcion/g" $archivo
    sed -i "s/CONTACTOS/$contactos/g" $archivo
    sed -i "s/MAPA/$mapa/g" $archivo
    sed -i "s/TWITTER/$twitter/g" $archivo
    sed -i "s/INSTAGRAM/$instagram/g" $archivo
    sed -i "s/FACEBOOK/$facebook/g" $archivo
    sed -i "s/TIKTOK/$tiktok/g" $archivo
    sed -i "s/DOMINIO/$dominio/g" $archivo

    sed -i "s/SINARROBAI/$sinarrobai/g" $archivo
    sed -i "s/SINARROBAT/$sinarrobat/g" $archivo
    sed -i "s/ARCHIVOD/$archivohtml/g" $archivo

    if [ -n "$foto" ]; then sed -i "s/mapa_asambleas.png/"$foto"/g" $archivo; fi;
    if [ -n "$instagram" ]; then link_ig=" <a href="https://www.instagram.com/$sinarrobai" title="Instagram" target="_blank">&nbsp;&nbsp;IG</a>"; fi;
    
    via="vía @MapadeAsambleas"
    texto_compartir="$nombre_asamblea - $horario $dir_asamblea - $localidad, $provincia. $descripcion $url $via"
    texto_compartir=$(echo "$texto_compartir" | sed 's/ /%20/g')
    archivo="tmp/filas.html"
    echo "<tr>" >> $archivo
    echo "<td>$provincia</td>" >> $archivo
    echo "<td>$localidad</td>" >> $archivo
    echo "<td><a href="$url" title='"$nombre_asamblea"'>$nombre_asamblea</a></td>" >> $archivo
    echo "<td>$dir_asamblea</td>" >> $archivo
    echo "<td>$horario</td>" >> $archivo
    echo "<td class="text-center"><a href='"https://twitter.com/intent/tweet?text=$texto_compartir"' title='Compartir por Twitter' class='TW icono' target='_blank'>X&nbsp;&nbsp;</a> $link_ig</td>" >> $archivo
    echo "</tr>" >> $archivo
}

# Descargar tsv
wget https://docs.google.com/spreadsheets/d/e/2PACX-1vTixuJvMIZjfRD8Vo9TUBd4aWqzB2rgjFgdGyXaGAwfLIWuj71bN5GI2DzC9g1ROuBSdtco6naDdvQq/pub?output=tsv -O mapa.tsv
# Crear directorio temporal
mkdir tmp
cp -r sitio/assets tmp
cp sitio/asambleas_mapa_sitio.html sitio/index.html sitio/google.html sitio/formulario.html sitio/acercade.html listado.html tmp
tail -n +2 mapa.tsv | sort -t$'\t' -k3,3 -k4,4 -k2,2 > mapa_ordenado.tsv
i=0
while read -r fila; do
    i=$((i+1))
    echo $i "- "$(echo "$fila" | awk -F'\t' '{print $17}')
    reemplazar "$fila"
done < mapa_ordenado.tsv
sed -i 's#\\/#/#g' tmp/filas.html
fila=$(grep -n "Filas" listado.html | awk -F ':' '{print $1}')
sed -i "${fila} r tmp/filas.html" "tmp/listado.html"
rm tmp/filas.html
#7z a "sitio_$(date +%Y%m%d_%H%M%S).7z" sitio # Opcional backup
rm -R sitio
mv tmp sitio
