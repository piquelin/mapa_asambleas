Para actualizar automáticamente la información de un mapa en Google My Maps desde un spreadsheet, puedes utilizar Google Apps Script. Aquí te muestro un ejemplo de cómo puedes hacerlo:

1. Abre tu spreadsheet en Google Sheets.

2. Ve a "Extensiones" en la barra de menú y selecciona "Apps Script".

3. En el editor de secuencia de comandos, pega el siguiente código:

```javascript
function actualizarMapaDesdeSpreadsheet() {
  // Obtener la hoja de cálculo activa y la hoja que contiene los datos del mapa
  var hojaDeCalculo = SpreadsheetApp.getActiveSpreadsheet();
  var hojaDatos = hojaDeCalculo.getSheetByName('Nombre de tu hoja de datos');
  
  // Obtener los datos del mapa
  var datos = hojaDatos.getDataRange().getValues();
  
  // Acceder al mapa de My Maps
  var idMapa = 'ID_de_tu_mapa';
  var mapa = Maps.newMap(idMapa);
  
  // Limpiar los marcadores existentes en el mapa
  mapa.clearMarkers();
  
  // Agregar nuevos marcadores al mapa
  for (var i = 1; i < datos.length; i++) {
    var latitud = datos[i][0]; // Suponiendo que la primera columna contiene latitudes
    var longitud = datos[i][1]; // Suponiendo que la segunda columna contiene longitudes
    var titulo = datos[i][2]; // Suponiendo que la tercera columna contiene títulos
    mapa.setMarker(latitud, longitud, titulo);
  }
  
  // Actualizar el mapa
  mapa.update();
}
```

Recuerda reemplazar `'Nombre de tu hoja de datos'` con el nombre de la hoja de tu spreadsheet que contiene los datos del mapa, y `'ID_de_tu_mapa'` con el ID único de tu mapa en Google My Maps.

Además, necesitarás configurar un activador de tiempo para que este script se ejecute automáticamente cada cierto tiempo. Puedes hacerlo yendo a "Editar" en la barra de menú, luego seleccionando "Activadores actuales". Haz clic en "Agregar activador", selecciona la función `actualizarMapaDesdeSpreadsheet`, establece el intervalo de tiempo deseado y guarda el activador.

### Error

   TypeError: Maps.newMap is not a function

### Referencias
- https://developers.google.com/apps-script/reference/maps?hl=es-419
- https://gemini.google.com/app
- https://console.cloud.google.com/google/maps-apis/api-list?project=mapa-de-las-asambleas

